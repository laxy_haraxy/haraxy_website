
// function findBrowser() {
//   var ua = navigator.userAgent, tem,
//   M = ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
//   if(/trident/i.test(M[1])){
//       tem =  /\brv[ :]+(\d+)/g.exec(ua) || [];
//       return 'IE '+( tem[1] || '');
//   }
//   if(M[1] === 'Chrome'){
//       tem = ua.match(/\b(OPR|Edge?)\/(\d+)/);
//       if(tem != null) return tem.slice(1).join(' ').replace('OPR', 'Opera').replace('Edg ', 'Edge ');            
//   }
//   M= M[2]? [M[1], M[2]]: [navigator.appName, navigator.appVersion, '-?'];
//   if((tem= ua.match(/version\/(\d+)/i))!= null) M.splice(1, 1, tem[1]);
//   return M.join(' ');
// }

// if(findBrowser() === 'Safari') {
//   document.querySelector('img').classList.add("inSafari"); 
// }


// video testimonials with image
$(".video-play").on('click', function (e) {
  e.preventDefault();
  var vidWrap = $(this).parent(),
    iframe = vidWrap.find('.video iframe'),
    iframeSrc = iframe.attr('src'),
    iframePlay = iframeSrc += "?autoplay=1";
  vidWrap.children('.video-thumbnail').fadeOut();
  vidWrap.children('.video-play').fadeOut();
  vidWrap.find('.video iframe').attr('src', iframePlay);
});


// video testimonial
$('.video-testimonial-owl').owlCarousel({
  items: 1.8,
  autoplay: true,
  margin: 50,
  thumbs: true,
  thumbImage: true,
  center: true,
  // slideTransition: 'linear',
  // autoplayTimeout: 5000,
  autoplaySpeed: 1000,
  autoplayHoverPause: true,
  loop: true,
  nav: false,
  responsive: {
    0: {
      items: 1,

    },
    700: {
      items: 1,

    },
    1000: {
      items: 1.8,

    },
    1200: {
      items: 1.8,

    }
  }
});
$('#gallery-carousel').owlCarousel({
  items: 5,
  autoplay: true,
  thumbs: true,
  thumbImage: true,
  center: true,
  lazyLoad: true,
  slideTransition: 'linear',
  autoplayTimeout: 3000,
  autoplaySpeed: 3000,
  autoplayHoverPause: false,
  loop: true,
  nav: false,
  dots: false,
  responsive: {
    0: {
      items: 2,

    },
    700: {
      items: 3,

    },
    1000: {
      items: 4,

    },
    1200: {
      items: 5,

    }
  }

});


// logo
$(document).ready(function () {
  var owl = $('#languages-logo');
  owl.owlCarousel({
    items: 6,
    loop: true,
    margin: 20,
    autoplay: true,
    slideTransition: 'linear',
    autoplayTimeout: 2000,
    autoplaySpeed: 2000,
    autoplayHoverPause: false,
    responsive: {
      0: {
        items: 2,

      },
      700: {
        items: 3,

      },
      1000: {
        items: 4,

      },
      1200: {
        items: 8,

      }
    }
  });

});



const body = document.body;
const triggerMenu = document.querySelector("header");
const scrollUp = "scroll-up";
const scrollDown = "scroll-down";
let lastScroll = 0;

triggerMenu.addEventListener("click", () => {
  body.classList.toggle("menu-open");
});

window.addEventListener("scroll", () => {
  const currentScroll = window.pageYOffset;
  if (currentScroll <= 0) {
    body.classList.remove(scrollUp);
    return;
  }

  if (currentScroll > lastScroll && !body.classList.contains(scrollDown)) {
    // down
    body.classList.remove(scrollUp);
    body.classList.add(scrollDown);
    // lottiePlayer.play();
  } else if (
    currentScroll < lastScroll &&
    body.classList.contains(scrollDown)
  ) {
    // up
    body.classList.remove(scrollDown);
    body.classList.add(scrollUp);
    // lottiePlayer.stop();
  }
  lastScroll = currentScroll;
});


jQuery('#file-upload').change(function () {
  var i = $(this).next('label').clone();
  var file = $('#file-upload')[0].files[0].name;
  $(this).next('label').text(file);
});
jQuery(".cookies-wrap .text-caption button.button").click(function () {
  jQuery(".cookies-wrap").hide();
});

jQuery('.open-bars-menu').click(function () {
  jQuery(this).toggleClass('active');
  jQuery('#overlay').toggleClass('open');
});
AOS.init();


jQuery(document).ready(function () {
  jQuery(".site-services-what-we-do-wrap .card-header button").click(function () {
    jQuery(".card").removeClass("current");
    if (jQuery(this).closest('.card').hasClass('current')) {
      console.log('if');

      jQuery(this).closest('.card').removeClass('current');
    }
    else {
      console.log('asdvc');
      jQuery(this).closest('.card').addClass('current');
    }
  });
});

$('#site-what-they-slider').owlCarousel({
  loop: true,
  margin: 20,
  nav: false,
  animateOut: 'fadeOut',
  animateIn: 'fadeIn',
  dots: true,
  autoplay: true,
  autoplayTimeout: 1000,
  autoplayHoverPause: true,
  responsiveClass: true,
  responsive: {
    0: {
      items: 1
    },
    600: {
      items: 1

    },
    1000: {
      items: 1
    }
  }
})


jQuery("button.navbar-toggler").click(function () {
  jQuery(".mobilemenu").addClass("menu-open");
  jQuery(".mobilemenu").removeClass("menu-close");
});
jQuery(".close-btn").click(function () {
  jQuery(".mobilemenu").addClass("menu-close");
  jQuery(".mobilemenu").removeClass("menu-open");
});

// Cookie
const cookieContainer = document.querySelector(".cookies-wrap");
const cookieButton = document.querySelector(".cookie-btn");

cookieButton.addEventListener("click", () => {
  cookieContainer.classList.remove("active");

  localStorage.setItem("cookieBannerDisplayed", "ture")
});

setTimeout(() => {
  if (!localStorage.getItem("cookieBannerDisplayed")) {
    cookieContainer.classList.add("active");
  }
}, 1000);

// work sticky carousel
$('#project_filter_car').owlCarousel({
  // items: 6,
  loop: true,
  dots: false,
  autoplayHoverPause: true,
  autoplay: false,
  autoplayTimeout: 2000,
  autoplaySpeed: 2000,

  responsive: {
    0: {
      items: 1,

    },
    700: {
      items: 3,

    },
    1000: {
      items: 4,

    },
    1200: {
      items: 6,

    }
  }
})

// sticky filter
var top;
var fixmeTop = $('.filter-main-wrap').offset().top;       // get initial position of the element

$(window).scroll(function () {                  // assign scroll event listener

  var currentScroll = $(window).scrollTop(); // get current position

  if (currentScroll >= fixmeTop) {           // apply position: fixed if you
    $('.filter-main-wrap').css({                      // scroll to that element or below it
      position: 'fixed',
      top: '0',
      left: '0',
      right: '0'
    });
  } else {                                   // apply position: static
    $('.filter-main-wrap').css({                      // if you scroll above it
      position: 'static'
    });
  }

})
// blog inner page theme change



